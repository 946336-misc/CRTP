#ifndef CRTP_COPY_946336
#define CRTP_COPY_946336

// Class Inner must have a copy constructor
template <class Inner>
class CRTP_copy
{
    public:
        CRTP_copy() {}
        virtual ~CRTP_copy() {}

        virtual Inner* clone() const {
            return new Inner(static_cast<const Inner&>(*this));
        }

    protected:

};

#endif

