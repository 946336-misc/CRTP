#ifndef CRTP_SINGLETON_946336
#define CRTP_SINGLETON_946336

template <class Inner>
class Singleton {
    public:
        static Inner &GetInstance() {
            if (p == nullptr) {
                p = new Inner;
            }
            return *p;
        }

    protected:
        static Inner *p;

    private:
        Singleton() {}
        Singleton(const Singleton &src);
        Singleton &operator=(const Singleton &src);
};

template <class Inner>
Inner *Singleton<Inner>::p = nullptr;

#endif


